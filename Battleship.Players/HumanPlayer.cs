﻿using System;
using System.Linq;
using Battleship.Enums;
using Battleship.TypeLib;
using Battleship.Interfaces;
using Battleship.Services;

namespace Battleship.Players
{
    public class HumanPlayer : Player
    {
        IDrawingService __DrawingService;
        ILocationService __LocationService;
        IInformationService __InformationService;
        public HumanPlayer(IDrawingService drawingService, ILocationService locationService, IInformationService informationService) : base()
        {
            PlayerType = PlayerType.Human;
            __DrawingService = drawingService ?? new DrawingService();
            __DrawingService.PlayerType = PlayerType;
            __InformationService = informationService ?? new InformationService();
            __LocationService = locationService ?? new LocationService(__InformationService);
        }

        public override ShotResult Attack(Player enemyPlayer)
        {
            bool isValidCoordinate = false;

            Coordinate attackCoordinate = __LocationService.GetCoordinate(PlayerType.Human);
            isValidCoordinate = ValidationService.ValidateCordinate(attackCoordinate.XCoordinate, attackCoordinate.YCoordinate, enemyPlayer.Grid.GridSize);

            while (!isValidCoordinate)
            {
                __InformationService.WriteLine("Invalid coordinate. Cordinate must be between A1 - J10. A-J is the rown name and 1-10 is the column number");
                attackCoordinate = __LocationService.GetCoordinate(PlayerType.Human);
                isValidCoordinate = ValidationService.ValidateCordinate(attackCoordinate.XCoordinate, attackCoordinate.YCoordinate, enemyPlayer.Grid.GridSize);
            }

            CursorPosition currentCursorPosition = __InformationService.GetCursorPosition();

            ShotResult result = Attack(attackCoordinate.YCoordinate, attackCoordinate.XCoordinate, enemyPlayer);

            __InformationService.SetCursorPosition(currentCursorPosition.Column, currentCursorPosition.Line);
            __InformationService.WriteLine(Environment.NewLine);
            return result;
        }

        public override ShotResult Attack(int y, int x, Player enemyPlayer)
        {
            CursorPosition cursorPosition;
            ShotResult shootResult = ShotResult.Miss;
            var enimyShipCoordinates = enemyPlayer.Grid.Ships.Select(ship => ship.ShipCoordinate);

            foreach (Ship ship in enemyPlayer.Grid.Ships)
            {
                foreach (Coordinate coordinate in ship.ShipCoordinate)
                {
                    if (coordinate.XCoordinate == x && coordinate.YCoordinate == y && coordinate.OccupationType != OccupationType.Hit)
                    {
                        coordinate.OccupationType = OccupationType.Hit;
                        enemyPlayer.Grid.GridCoordinate[y, x].OccupationType = OccupationType.Hit;
                        cursorPosition = coordinate.CursorPosition;
                        __DrawingService.Draw(coordinate.OccupationType, cursorPosition);
                        ship.Hit += 1;
                        return ShotResult.Hit;
                    }
                }
            }

            enemyPlayer.Grid.GridCoordinate[y, x].OccupationType = OccupationType.Miss;
            cursorPosition = enemyPlayer.Grid.GridCoordinate[y, x].CursorPosition;
            __DrawingService.Draw(OccupationType.Miss, cursorPosition);
            return shootResult;
        }

        public override bool DeployShips()
        {
            throw new NotImplementedException();
        }

        public override bool DrawBoard()
        {
            throw new NotImplementedException();
        }

        private Coordinate GetCoordinate(string userInput)
        {
            int xCoordinate = 10;
            int yCoordinate = 10;
            char y = userInput.Substring(0, 1).ToCharArray().First();
            string x = userInput.Substring(1, userInput.Length - 1);
            if (userInput.Length < 4)
            {
                if (x.Length > 0 && x.ToUpper().All(char.IsNumber))
                {
                    xCoordinate = int.Parse(x) - 1;
                }
                else
                {
                    xCoordinate = 10;
                }

                if (y.ToString().ToUpper().All(char.IsLetter))
                {
                    yCoordinate = (char.ToUpper(y) - 64) - 1;
                }
                else
                {
                    yCoordinate = 10;
                }
            }
            return new Coordinate(xCoordinate, yCoordinate);
        }
    }
}