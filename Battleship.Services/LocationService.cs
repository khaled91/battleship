﻿using System;
using Battleship.Enums;
using Battleship.Interfaces;
using Battleship.TypeLib;
using System.Linq;

namespace Battleship.Services
{
    public class LocationService : ILocationService
    {
        IInformationService __InformationService;
        public LocationService(IInformationService informationService)
        {
            __InformationService = informationService;
        }

        public Coordinate GetCoordinate(PlayerType playerType)
        {
            if (playerType == PlayerType.Computer)
            {
                return GetRandomCoordinate();
            }
            return GetManualCoordinate();
        }

        private Coordinate GetRandomCoordinate()
        {
            Coordinate coordinate;
            int xCoordinate = 10;
            int yCoordinate = 10;

            xCoordinate = GetRandom(9);
            yCoordinate = GetRandom(9);
            coordinate = new Coordinate(xCoordinate, yCoordinate);
            return coordinate;
        }

        private Coordinate GetManualCoordinate()
        {
            string readline;
            readline = __InformationService.Read("Enter a Coordinate: ");

            Coordinate coordinate = GetCoordinate(readline);
            return coordinate;
        }

        private Coordinate GetCoordinate(string userInput)
        {
            int xCoordinate = 10;
            int yCoordinate = 10;

            char y = userInput.Substring(0, 1).ToCharArray().First();
            string x = userInput.Substring(1, userInput.Length - 1);
            if (userInput.Length < 4)
            {
                if (x.Length > 0 && x.ToUpper().All(char.IsNumber))
                {
                    xCoordinate = int.Parse(x) - 1;
                }
                else
                {
                    xCoordinate = 10;
                }

                if (y.ToString().ToUpper().All(char.IsLetter))
                {
                    yCoordinate = (char.ToUpper(y) - 64) - 1;
                }
                else
                {
                    yCoordinate = 10;
                }
            }
            return new Coordinate(xCoordinate, yCoordinate);
        }

        public Orientation GetOrientation()
        {
            return (Orientation)GetRandom(1);
        }

        public int GetRandom(int maxNumber)
        {
            Random rand = new Random();
            return rand.Next(maxNumber);
        }

        public Coordinate GetShipCordinate(PlayerType playerType, int shipID)
        {
            return GetCoordinate(PlayerType.Computer);
        }
    }
}