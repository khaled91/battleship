﻿using System;
using Battleship.Enums;
using Battleship.TypeLib;
using Battleship.Interfaces;

namespace Battleship.Services.Players
{
    public class ComputerPlayer : Player
    {
        IDrawingService __DrawingService;
        ILocationService __LocationService;
        IInformationService __InformationService;
        public ComputerPlayer(IDrawingService drawingService, ILocationService locationService, IInformationService informationService) : base()
        {
            PlayerType = PlayerType.Computer;
            __DrawingService = drawingService;
            __DrawingService.PlayerType = PlayerType;
            __InformationService = informationService;
            __LocationService = locationService;
        }

        public override ShotResult Attack(Player enemyPlayer)
        {
            throw new NotImplementedException();
        }

        public override ShotResult Attack(int y, int x, Player enemyPlayer)
        {
            throw new NotImplementedException();
        }

        public override bool DeployShips()
        {
            try
            {
                foreach (Ship ship in Grid.Ships)
                {
                    int startXCoordinate = 0;
                    int startYCoordinate = 0;
                    bool isValid = false;

                    while (!isValid)
                    {
                        ship.Orientation = __LocationService.GetOrientation();
                        Coordinate coordinate = __LocationService.GetShipCordinate(PlayerType.Computer, ship.ID);
                        startXCoordinate = coordinate.XCoordinate;
                        startYCoordinate = coordinate.YCoordinate;
                        isValid = ValidationService.IsDeploymentCoordinateValid(coordinate, ship, Grid);
                    }

                    int xCoordinate = startXCoordinate;
                    int yCoordinate = startYCoordinate;
                    ship.ShipCoordinate = new Coordinate[ship.Length];

                    for (int shiplen = 0; shiplen < ship.Length; shiplen++)
                    {
                        CursorPosition cursorPosition = Grid.GridCoordinate[yCoordinate, xCoordinate].CursorPosition;
                        ship.ShipCoordinate[shiplen] = new Coordinate()
                        {
                            XCoordinate = xCoordinate,
                            YCoordinate = yCoordinate,
                            CursorPosition = cursorPosition,
                            OccupationType = OccupationType.Ship
                        };
                        __DrawingService.Draw(OccupationType.Ship, cursorPosition);
                        if (ship.Orientation == Orientation.Vertical)
                        {
                            if (startYCoordinate > 5)
                            {
                                yCoordinate -= 1;
                            }
                            else
                            {
                                yCoordinate += 1;
                            }
                        }
                        else
                        {
                            if (startXCoordinate > 5)
                            {
                                xCoordinate -= 1;
                            }
                            else
                            {
                                xCoordinate += 1;
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                __InformationService.WriteLine($"Unable to deploy ships. Error: {ex.Message}.");
                return false;
            }
            return true;
        }

        public override bool DrawBoard()
        {
            try
            {
                Grid.GridPosition = __InformationService.GetCursorPosition();
                for (int row = 0; row < Grid.GridSize; row++)
                {
                    for (int column = 0; column < Grid.GridSize; column++)
                    {
                        __DrawingService.Draw(OccupationType.Empty, null);
                        Grid.GridCoordinate[row, column] = new Coordinate(column, row)
                        {
                            CursorPosition = __InformationService.GetCursorPosition(),
                            OccupationType = OccupationType.Empty
                        };
                    }
                    __InformationService.WriteLine(Environment.NewLine);
                }
            }
            catch (Exception ex)
            {
                __InformationService.WriteLine($"Unable to draw batlleship board. Error:{ex.Message}.");
                return false;
            }
            return true;
        }
    }
}