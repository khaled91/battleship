﻿using Battleship.Enums;
using Battleship.TypeLib;

namespace Battleship.Services.Players
{
    public abstract class Player
    {
        public abstract ShotResult Attack(Player enemyPlayer);
        public abstract ShotResult Attack(int y, int x, Player enemyPlayer);
        public abstract bool DeployShips();
        public abstract bool DrawBoard();
        public Grid Grid { get; set; }
        public bool IsDefeated { get; set; }
        public PlayerType PlayerType { get; set; }
    }
}