﻿using Battleship.Enums;
using Battleship.TypeLib;
using System.Linq;

namespace Battleship.Services
{
    public static class ValidationService
    {
        private const int MAX_START_COORDINATE = 5;
        public static bool ValidateCordinate(int xCoordinate, int yCoordinate, int battleshipSize)
        {
            return xCoordinate < battleshipSize && yCoordinate < battleshipSize;
        }

        public static bool IsDeploymentCoordinateValid(Coordinate coordinate, Ship ship, Grid grid)
        {
            bool isValid = false;
            Coordinate[] coord = new Coordinate[ship.Length];
            int startXCoordinate = coordinate.XCoordinate;
            int startYCoordinate = coordinate.YCoordinate;
            int xCord = startXCoordinate;
            int yCord = startYCoordinate;

            System.Collections.Generic.IEnumerable<Coordinate[]> DeployedShipsCoordinate = grid.Ships.Where(x => x.ShipCoordinate != null).Select(a => a.ShipCoordinate);

            for (int l = 0; l < ship.Length; l++)
            {
                coord[l] = new Coordinate()
                {
                    XCoordinate = xCord,
                    YCoordinate = yCord
                };
                if (ship.Orientation == Orientation.Vertical)
                {
                    if (startYCoordinate > MAX_START_COORDINATE)
                    {
                        yCord -= 1;
                    }
                    else
                    {
                        yCord += 1;
                    }
                }
                else
                {
                    if (startXCoordinate > MAX_START_COORDINATE)
                    {
                        xCord -= 1;
                    }
                    else
                    {
                        xCord += 1;
                    }
                }
            }
            isValid = true;
            foreach (Coordinate[] cordinateList in DeployedShipsCoordinate)
            {
                foreach (Coordinate c in coord)
                {
                    if (cordinateList.Any(x => x.XCoordinate == c.XCoordinate && x.YCoordinate == c.YCoordinate))
                    {
                        isValid = false;
                    }
                }
            }
            return isValid;
        }
    }
}