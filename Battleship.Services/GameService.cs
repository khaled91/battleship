﻿using Battleship.Enums;
using Battleship.Interfaces;
using Battleship.Services.Players;
using Battleship.TypeLib;
using System;
using System.Linq;

namespace Battleship.Services
{
    public class GameService
    {
        Player __HumanPlayer;
        Player __ComputerPlayer;
        IInformationService __InformationService;
        public GameService(Player computerPlayer, Player humanPlayer, IInformationService informationService)
        {
            __ComputerPlayer = computerPlayer;
            __HumanPlayer = humanPlayer;
            __InformationService = informationService;
        }

        public void Play()
        {
            try
            {
                __InformationService.WriteLine("Computer Player:");
                __InformationService.WriteLine("---------------------");

                if (!__ComputerPlayer.DrawBoard())
                {
                    __InformationService.WriteLine("Failed to draw battleship. Aborting the game.");
                    return;
                }

                if (!__ComputerPlayer.DeployShips())
                {
                    __InformationService.WriteLine("Failed to deploy ships. Aborting the game.");
                    return;
                }

                CursorPosition cursor = __ComputerPlayer.Grid.GridCoordinate[__ComputerPlayer.Grid.GridSize - 1, __ComputerPlayer.Grid.GridSize - 1].CursorPosition;

                __InformationService.SetCursorPosition(cursor.Column, cursor.Line);
                __InformationService.WriteLine(Environment.NewLine);
                while (!IsGameOver)
                {
                    ShotResult shootResult = ShotResult.Hit;
                    while (!IsGameOver)
                    {
                        while (shootResult == ShotResult.Hit || !IsGameOver)
                        {
                            shootResult = __HumanPlayer.Attack(__ComputerPlayer);
                            if (IsGameOver) { break; }
                        }
                        break;
                    }
                    if (IsGameOver) { break; }
                    shootResult = ShotResult.Hit;
                }
                if (IsGameOver)
                {
                    __InformationService.WriteLine("Congratulation you have won!!!");
                }

                __InformationService.Read("Press anykey to exist the game.");
            }
            catch (Exception ex)
            {
                __InformationService.WriteLine($"An exception has been thrown. Error: {ex.Message}.");
            }
        }

        public bool IsGameOver
        {
            get
            {
                return __ComputerPlayer.Grid.Ships.All(x => x.IsSunk == true);
            }
        }
    }
}