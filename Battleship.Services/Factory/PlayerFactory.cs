﻿using Battleship.Enums;
using Battleship.Interfaces;
using Battleship.Services.Players;
using Battleship.TypeLib;
using System.Collections.Generic;

namespace Battleship.Services.Factory
{
    public class PlayerFactory
    {
        private static int GRID_SIZE = 10;
        IDrawingService __DrawingService;
        IInformationService __InformationService;
        ILocationService __LocationService;

        public PlayerFactory(IDrawingService drawingService, IInformationService informationService, ILocationService locationService)
        {
            __DrawingService = drawingService;
            __InformationService = informationService;
            __LocationService = locationService;
        }

        public Player CreatePlayer(PlayerType playerType)
        {
            ShipFactory shipFactory = new ShipFactory();
            Player player;
            switch (playerType)
            {
                case PlayerType.Computer:
                    player = new ComputerPlayer(__DrawingService, __LocationService, __InformationService);
                    break;
                case PlayerType.Human:
                    player = new HumanPlayer(__DrawingService, __LocationService, __InformationService);
                    break;
                default:
                    return null;
            }

            List<Ship> shipList = new List<Ship>()
            {
                shipFactory.CreateShip(1, ShipType.Battleship),
                shipFactory.CreateShip(1, ShipType.Destroyer),
                shipFactory.CreateShip(1, ShipType.Destroyer)
            };
            player.Grid = new Grid() { GridSize = GRID_SIZE, Ships = shipList, GridCoordinate = new Coordinate[GRID_SIZE, GRID_SIZE] };
            return player;
        }
    }
}
