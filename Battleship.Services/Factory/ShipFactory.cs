﻿using Battleship.Enums;
using Battleship.TypeLib;

namespace Battleship.Services.Factory
{
    public class ShipFactory
    {
        public ShipFactory()
        {

        }

        public Ship CreateShip(int id, ShipType shipType)
        {
            Ship ship;
            switch (shipType)
            {
                case ShipType.Battleship:
                    ship = new TypeLib.Battleship() { ID = id };
                    break;
                case ShipType.Destroyer:
                    ship = new Destroyer() { ID = id };
                    break;
                default:
                    ship = null;
                    break;
            }
            return ship;
        }
    }
}
