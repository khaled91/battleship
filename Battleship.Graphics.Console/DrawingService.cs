﻿using Battleship.Enums;
using Battleship.Enums.Extension;
using Battleship.Interfaces;
using Battleship.TypeLib;
using System;

namespace Battleship.Graphics.Console
{
    public class DrawingService : IDrawingService
    {
        public DrawingService()
        {
        }

        public void Draw(OccupationType occupationType, CursorPosition cursorPosition)
        {
            switch (occupationType)
            {
                case OccupationType.Hit:
                    DrawHit(cursorPosition);
                    break;
                case OccupationType.Miss:
                    DrawMiss(cursorPosition);
                    break;
                case OccupationType.Ship:
                    DrawShip(cursorPosition);
                    break;
                case OccupationType.Empty:
                default:
                    DrawEmpty(cursorPosition);
                    break;
            }
        }

        private void DrawEmpty(CursorPosition cursorPosition)
        {
            System.Console.Write("   {0}", OccupationType.Empty.GetDescription());
        }

        private void DrawShip(CursorPosition cursorPosition)
        {
            System.Console.SetCursorPosition(cursorPosition.Column, cursorPosition.Line);
            System.Console.Write("\b{0}", PlayerType == PlayerType.Human ? OccupationType.Ship.GetDescription() : OccupationType.Empty.GetDescription());
        }

        private void DrawHit(CursorPosition cursorPosition)
        {
            System.Console.SetCursorPosition(cursorPosition.Column, cursorPosition.Line);
            System.Console.Write("\b{0}", OccupationType.Hit.GetDescription());
        }

        private void DrawMiss(CursorPosition cursorPosition)
        {
            System.Console.SetCursorPosition(cursorPosition.Column, cursorPosition.Line);
            System.Console.Write("\b{0}", OccupationType.Miss.GetDescription());
        }

        public PlayerType PlayerType { get; set; }
    }
}