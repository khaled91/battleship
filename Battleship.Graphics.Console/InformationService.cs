﻿using Battleship.Interfaces;
using Battleship.TypeLib;

namespace Battleship.Graphics.Console
{
    public class InformationService : IInformationService
    {
        public CursorPosition GetCursorPosition()
        {
            return new CursorPosition(System.Console.CursorTop, System.Console.CursorLeft);
        }

        public string Read(string readQuestion)
        {
            System.Console.Write(readQuestion);
            return System.Console.ReadLine();
        }

        public void SetCursorPosition(int column, int row)
        {
            System.Console.SetCursorPosition(column, row);
        }

        public void Write(string message)
        {
            System.Console.Write(message);
        }

        public void WriteLine(string message)
        {
            System.Console.WriteLine(message);
        }
    }
}