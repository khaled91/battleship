﻿using Battleship.Enums;
using Battleship.Services.Factory;
using Battleship.Interfaces;
using Battleship.Services.Players;
using Battleship.TypeLib;
using Moq;
using System.Collections.Generic;

namespace Battleship.Test.Unit
{
    public class BattleshipTestBase
    {
        public const int BATTLESHIP_SHIZE = 10;
        public Mock<IDrawingService> mockDrawingService = new Mock<IDrawingService>();
        public Mock<IInformationService> mockInformationService = new Mock<IInformationService>();
        public Mock<ILocationService> mockLocationService = new Mock<ILocationService>();
        public Mock<IShip> mockShip = new Mock<IShip>();
        public Player computerPlayer;
        public Player humanPlayer;
        public ShipFactory shipFactory = new ShipFactory();

        public BattleshipTestBase()
        {

        }

        public void InitialiseTest()
        {
            mockInformationService.Setup(x => x.GetCursorPosition()).Returns(new CursorPosition(11, 0));
            mockDrawingService.Setup(x => x.Draw(It.IsAny<OccupationType>(), It.IsAny<CursorPosition>())).Callback(() => { });
            mockLocationService.Setup(x => x.GetOrientation()).Returns(Orientation.Vertical);
            mockLocationService.Setup(x => x.GetShipCordinate(PlayerType.Computer, 1)).Returns(new Coordinate(0, 0));
            mockLocationService.Setup(x => x.GetShipCordinate(PlayerType.Computer, 2)).Returns(new Coordinate(1, 0));
            mockLocationService.Setup(x => x.GetShipCordinate(PlayerType.Computer, 3)).Returns(new Coordinate(2, 0));

            humanPlayer = new HumanPlayer(mockDrawingService.Object, mockLocationService.Object, mockInformationService.Object);
            computerPlayer = new ComputerPlayer(mockDrawingService.Object, mockLocationService.Object, mockInformationService.Object);

            List<Ship> shipList = new List<Ship>()
            {
                shipFactory.CreateShip(1, ShipType.Battleship),
                shipFactory.CreateShip(2, ShipType.Destroyer),
                shipFactory.CreateShip(3, ShipType.Destroyer)
            };

            computerPlayer.Grid = new Grid() { GridSize = BATTLESHIP_SHIZE, Ships = shipList, GridCoordinate = new Coordinate[BATTLESHIP_SHIZE, BATTLESHIP_SHIZE] };
            computerPlayer.DrawBoard();
            computerPlayer.DeployShips();
        }
    }
}
