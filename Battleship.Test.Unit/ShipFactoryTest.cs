﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleship.Services.Factory;
using Battleship.Enums;
using Battleship.TypeLib;

namespace Battleship.Test.Unit
{
    [TestClass]
    public class ShipFactoryTest : BattleshipTestBase
    {
        [TestInitialize]
        public void Intialise()
        {
            InitialiseTest();
        }

        [TestMethod]
        public void ShipFctory_CreateShip_ShouldReturnBattleship()
        {
            Ship ship = shipFactory.CreateShip(1, ShipType.Battleship);
            Assert.AreEqual(ship.GetType(), typeof(TypeLib.Battleship));
        }


        [TestMethod]
        public void ShipFctory_CreateShip_ShouldReturnDestroyer()
        {
            Ship ship = shipFactory.CreateShip(2, ShipType.Destroyer);
            Assert.AreEqual(ship.GetType(), typeof(TypeLib.Destroyer));
        }
    }
}
