﻿using Battleship.Enums;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Battleship.Test.Unit
{
    [TestClass]
    public class PlayerTest : BattleshipTestBase
    {
        [TestInitialize]
        public void Initialise()
        {
            base.InitialiseTest();   
        }

        [TestMethod]
        public void HumanPlayer_Attack_ShouldReturnAttackResultHit()
        {
            ShotResult result = humanPlayer.Attack(0, 1, computerPlayer);
            Assert.AreEqual(ShotResult.Hit, result);
            Assert.AreEqual(1,computerPlayer.Grid.Ships.Where(x => x.ID == 2).Single().Hit);
            Assert.AreEqual(OccupationType.Hit, computerPlayer.Grid.Ships.Where(x => x.ID == 2).Single().ShipCoordinate[0].OccupationType);
        }

        [TestMethod]
        public void HumanPlayer_Attack_ShouldReturnAttackResultMiss()
        {
            ShotResult result = humanPlayer.Attack(0, 4, computerPlayer);
            Assert.AreEqual(ShotResult.Miss, result);
            Assert.IsTrue(computerPlayer.Grid.Ships.All(x => x.Hit == 0));
            Assert.IsTrue(computerPlayer.Grid.Ships.All(x => x.ShipCoordinate.All(y => y.OccupationType == OccupationType.Ship)));
        }
    }
}
