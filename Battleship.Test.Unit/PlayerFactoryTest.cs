﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Battleship.Services.Factory;
using Battleship.Services.Players;
using Battleship.Enums;

namespace Battleship.Test.Unit
{
    [TestClass]
    public class PlayerFactoryTest : BattleshipTestBase
    {
        PlayerFactory playerFactory;
        [TestInitialize]
        public void Initialise()
        {
            base.InitialiseTest();
            playerFactory = new PlayerFactory(mockDrawingService.Object, mockInformationService.Object, mockLocationService.Object);
        }

        [TestMethod]
        public void PlayerFacotry_CreatePlayer_ShouldReturnComputerPlayer()
        {
            Player player = playerFactory.CreatePlayer(PlayerType.Computer);
            Assert.AreEqual(player.GetType(), typeof(ComputerPlayer));
        }

        [TestMethod]
        public void PlayerFacotry_CreatePlayer_ShouldReturnComputerPlayerWithGridSizeTen()
        {
            Player player = playerFactory.CreatePlayer(PlayerType.Computer);
            Assert.AreEqual(player.GetType(), typeof(ComputerPlayer));
            Assert.AreEqual(BATTLESHIP_SHIZE, player.Grid.GridSize);
        }

        [TestMethod]
        public void PlayerFacotry_CreatePlayer_ShouldReturnHumanPlayer()
        {
            Player player = playerFactory.CreatePlayer(PlayerType.Human);
            Assert.AreEqual(player.GetType(), typeof(HumanPlayer));
        }

        [TestMethod]
        public void PlayerFacotry_CreatePlayer_ShouldReturnPlayerAsNull()
        {
            
            Player player = playerFactory.CreatePlayer((PlayerType)3);
            Assert.AreEqual(player, null);
        }
    }
}
