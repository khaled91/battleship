﻿using Battleship.Enums;
using Battleship.Services;
using Battleship.Services.Players;
using Battleship.TypeLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace Battleship.Test.Unit
{
    [TestClass]
    public class GameSeriveTest : BattleshipTestBase
    {
        [TestInitialize]
        public void Initialise()
        {
            InitialiseTest();
        }

        [TestMethod]
        public void GameService_IsGameOver_ShouldReturnTrue()
        {
            HitAllShipCoordinateExceptOne(computerPlayer);
            GameService gameService = new GameService(computerPlayer, humanPlayer, mockInformationService.Object);
            ShotResult shootResult = humanPlayer.Attack(1, 0, computerPlayer);
            gameService = new GameService(computerPlayer, humanPlayer, mockInformationService.Object);
            bool result = gameService.IsGameOver;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void GameService_IsGameOver_ShouldReturnFalse()
        {
            HitAllShipCoordinateExceptOne(computerPlayer);
            GameService gameService = new GameService(computerPlayer, humanPlayer, mockInformationService.Object);
            ShotResult shootResult = humanPlayer.Attack(9, 9, computerPlayer);
            bool result = gameService.IsGameOver;
            Assert.IsFalse(result);
        }

        private void HitAllShipCoordinateExceptOne(Player enemyPlayer)
        {
            foreach (Ship ship in enemyPlayer.Grid.Ships)
            {
                foreach (Coordinate coordinate in ship.ShipCoordinate)
                {
                    coordinate.OccupationType = OccupationType.Hit;
                    ship.Hit += 1;
                }
            }

            Ship battleship = enemyPlayer.Grid.Ships.Where(x => x.ID == 1).Single();
            foreach (Coordinate coordinate in battleship.ShipCoordinate)
            {
                if (coordinate.XCoordinate == 0 && coordinate.YCoordinate == 1)
                {
                    coordinate.OccupationType = OccupationType.Ship;
                    battleship.Hit -= 1;
                }
            }
        }
    }
}

