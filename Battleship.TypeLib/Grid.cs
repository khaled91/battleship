﻿using System.Collections.Generic;

namespace Battleship.TypeLib
{
    public class Grid
    {
        public Coordinate[,] GridCoordinate { get; set; }
        public CursorPosition GridPosition { get; set; }
        public int GridSize { get; set; }
        public List<Ship> Ships { get; set; }
    }
}