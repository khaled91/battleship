﻿using Battleship.Enums;

namespace Battleship.TypeLib
{
    public class Coordinate
    {
        public Coordinate() { }
        public Coordinate(int xCoordinate, int yCoordinate)
        {
            XCoordinate = xCoordinate;
            YCoordinate = yCoordinate;
        }
        public CursorPosition CursorPosition { get; set; }
        public OccupationType OccupationType { get; set; }
        public int XCoordinate { get; set; }
        public int YCoordinate { get; set; }
    }
}