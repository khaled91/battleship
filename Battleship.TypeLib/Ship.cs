﻿using Battleship.Enums;

namespace Battleship.TypeLib
{
    public class Ship
    {
        public int ID { get; set; }
        public int Hit { get; set; }
        public bool IsSunk { get { return Hit >= Length; } }
        public int Length { get; internal set; }
        public string Name { get; internal set; }
        public Orientation Orientation { get; set; }
        public Coordinate[] ShipCoordinate { get; set; }
        public ShipType ShipType { get; set; }
    }
}