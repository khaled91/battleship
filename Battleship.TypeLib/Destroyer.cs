﻿using Battleship.Enums;
using Battleship.Enums.Extension;

namespace Battleship.TypeLib
{
    public class Destroyer : Ship
    {
        public Destroyer()
        {
            Length = (int)ShipType.Destroyer;
            Name = ShipType.Destroyer.GetDescription();
            ShipType = ShipType.Destroyer;
        }
    }
}