﻿using Battleship.Enums;
using Battleship.Enums.Extension;

namespace Battleship.TypeLib
{
    public class Battleship : Ship
    {
        public Battleship()
        {
            Length = (int)ShipType.Battleship;
            Name = ShipType.Battleship.GetDescription();
            ShipType = ShipType.Battleship;
        }
    }
}