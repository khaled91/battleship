﻿namespace Battleship.TypeLib
{
    public class CursorPosition
    {
        public CursorPosition(int line, int column)
        {
            Column = column;
            Line = line;
        }
        public int Column { get; set; }
        public int Line { get; set; }
    }
}