﻿using Battleship.Interfaces;
using Battleship.Services;
using Ninject.Modules;

namespace Battleship.IOC
{
    public class ServiceModules : NinjectModule
    {
        public override void Load()
        {
            Bind<GameService>().To<GameService>();
            Bind<ILocationService>().To<LocationService>();
        }
    }
}
