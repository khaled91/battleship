﻿using System.ComponentModel;
namespace Battleship.Enums
{
    public enum PlayerType : int
    {
        Human,
        Computer
    }

    public enum Orientation : int
    {
        Vertical,
        Horizontal
    }

    public enum ShipType : int
    {
        [Description("Battleship")]
        Battleship = 5,
        [Description("Destroyer")]
        Destroyer = 4
    }

    public enum ShotResult : int
    {
        Hit,
        Miss
    }

    public enum OccupationType : int
    {
        [Description("*")]
        Empty,
        [Description("H")]
        Hit,
        [Description("M")]
        Miss,
        [Description("S")]
        Ship
    }
}