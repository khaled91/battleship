﻿using Ninject.Modules;
using Battleship.Interfaces;
using Battleship.Console.Services;
using Ninject;

namespace Battleship.Console.Ioc
{
    public class ConsoleModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IInformationService>().To<InformationService>();
            Bind<IDrawingService>().To<DrawingService>();
            Kernel.Load(new IOC.ServiceModules());
        }
    }
}
