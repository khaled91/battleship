﻿using Battleship.Enums;
using Battleship.TypeLib;

namespace Battleship.Interfaces
{
    public interface IDrawingService
    {
        void Clear(CursorPosition fromPosition, int lineOffset);
        void Draw(OccupationType occupationType, CursorPosition cursorPosition);
        PlayerType PlayerType { get; set; }
    }
}