﻿using Battleship.Enums;
using Battleship.TypeLib;

namespace Battleship.Interfaces
{
    public interface IShip
    {
        int Hit { get; set; }
        bool IsSunk { get; }
        int Length { get; }
        string Name { get; }
        Orientation Orientation { get; }
        Coordinate[] ShipCoordinate { get; }
        ShipType ShipType { get; set; }
    }
}
