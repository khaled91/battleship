﻿using Battleship.TypeLib;

namespace Battleship.Interfaces
{
    public interface IInformationService
    {
        string Read(string readQuestion);
        void Write(string message);
        void WriteLine(string message);
        void SetCursorPosition(int column, int row);
        CursorPosition GetCursorPosition();
    }
}