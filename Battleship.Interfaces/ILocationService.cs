﻿using Battleship.Enums;
using Battleship.TypeLib;

namespace Battleship.Interfaces
{
    public interface ILocationService
    {
        Coordinate GetCoordinate(PlayerType playerType);
        Coordinate GetShipCordinate(PlayerType playerType, int shipID);
        Orientation GetOrientation();
    }
}