﻿using Battleship.Enums;
using Battleship.Services.Factory;
using Battleship.Interfaces;
using Battleship.Services.Players;
using Battleship.Services;
using Battleship.Console.Ioc;
using Ninject;

namespace Battleship.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            DisplayIntro();
            Play();
        }

        private static void DisplayIntro()
        {
            System.Console.WriteLine("\t\tWelcome to the game of Battleship");
            System.Console.WriteLine("\t\t=================================");
            System.Console.WriteLine();
            System.Console.WriteLine("A game by Khaled Ahmed");
            System.Console.WriteLine("The purpose of this game is for the human player to guess and destroy the ships that is hidden in the Computer players battleship board.");
            System.Console.WriteLine("A-J is row name and 1-10 is column name. To hit frist top left coordinate, you enter A1 and to hit the bottom right coordinate you enter J10.");
            System.Console.WriteLine();
        }

        private static void Play()
        {
            IKernel kernel = LoadKernel();
            IDrawingService drawingService = kernel.Get<IDrawingService>();
            IInformationService informationService = kernel.Get<IInformationService>();
            ILocationService locationService = kernel.Get<ILocationService>();
            PlayerFactory playerFactory = new PlayerFactory(drawingService, informationService, locationService);
            Player humanPlayer = playerFactory.CreatePlayer(PlayerType.Human);
            Player computerPlayer = playerFactory.CreatePlayer(PlayerType.Computer);

            GameService game = new GameService(computerPlayer, humanPlayer, informationService);
            game.Play();
        }

        private static IKernel LoadKernel()
        {
            return new StandardKernel(new ConsoleModule());
        }
    }
}